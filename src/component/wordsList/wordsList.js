import {observer} from "mobx-react";
import GlobalStore from "../../store/store";
import {Container} from "../../page/home/styles";

const WordsList = observer(() => {
    return  GlobalStore.activeList.map((item, index) => (
            <Container row key={index + item}>
                <div><p>{item}</p></div>
            </Container>
        ))
})

export default WordsList