import {useEffect, useState} from "react";
import {observer} from "mobx-react";
import GlobalStore from "../../store/store";
import {Container, InputStyle, TextStyle} from "./styles";
import WordsList from "../../component/wordsList/wordsList";


const Home = observer(() => {
    const [searchWord, setSearchWord] = useState('')
    const [searchLetter, setSearchLetter] = useState('')
    useEffect(() => {
        GlobalStore.fetch()
    }, [])


    const _setWord = ({target}) => {
        setSearchWord(target.value)
        GlobalStore.searchList(target.value)

    }
    const _setLetter = ({target = []}) => {
        GlobalStore.setLetter(target.value.length > 1 ? target.value[0] : target.value)
        setSearchLetter(target.value.length > 1 ? target.value[0] : target.value)
    }

    return <div>
        <Container row>
            <p> Search</p>
            <InputStyle value={searchWord}
                        onChange={_setWord}/>

        </Container>
        <Container row>
            <Container row margin={' auto auto auto 0'}>
                <InputStyle value={searchLetter}
                            placeholder={'input letter'}
                            onChange={_setLetter}/>
            </Container>
            <Container column>
                <TextStyle> {`${GlobalStore.getCountStartLetter} words start with the letter ${searchLetter}`}</TextStyle>
                <TextStyle> {`${GlobalStore.getCountAllLetterRepeat} include in words letter ${searchLetter}`}</TextStyle>
                <TextStyle> {` matches letters "${searchLetter}" at the end of a word  ${GlobalStore.getEndLetter}`}</TextStyle>
                <TextStyle>{`${GlobalStore.getCountDuplicateLetter} the number of repetitions of additional letters ${searchLetter}`}</TextStyle>
            </Container>
        </Container>
        <Container row>
            <p>words:</p>
        </Container>
        <WordsList/>
    </div>
})

export default Home