import styled from "@emotion/styled";

export const Container = styled.div(props => ({
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'left',
    flexDirection: props.column && 'column',
    maxWidth: '800px',
    margin: props.margin ? props.margin: 'auto'
}))
export const InputStyle = styled.input(() => ({
    width: '80%',
    height: '30px',
    fontSize: '17px',
    margin: 'auto 0'

}))
export const TextStyle = styled.p(() => ({
    textAlign: 'left',
}))