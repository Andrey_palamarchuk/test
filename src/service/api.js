const jsonHeaders = {
    'Accept': 'application/json',
    "app_id ":"a80d3c3f",
    "app_key": "d53d025ea265178b276c58b05a035c8e",
};


const defautCustomOptions = {
    includeAuthHeader: true,
    includeCredentials: true,
};


export async function get(
    path,
    body = {},
    headers = {},
    options = {},
    customOptions = defautCustomOptions,
) {
    const request = await fetchEnriched(
        'GET',
        path,
        null,
        getHeaders(jsonHeaders, headers),
        options,
        customOptions,
    );
    try {
        if (!request.ok) {
            const err = await request.json();
            throw Error(err.error);
        }
        return request.json();
    } catch (err) {
        throw err;
    }
}


function fetchEnriched(method, path, body, headers, options) {
    const endpointUrl = getApiUrl(path);
    return fetch(endpointUrl, {
        method: method,
        headers,
        body: body,
        ...options,
    });
}

function getApiUrl(path) {
    return 'https://od-api.oxforddictionaries.com/api/v2'+path;
}

function getHeaders(defaultHeaders, headers) {
    return {
        ...defaultHeaders,
        ...headers,
    };
}