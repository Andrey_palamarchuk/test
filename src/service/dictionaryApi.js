import {get} from './api'

export const searchWord = (word) => {
    return get('/search/thesaurus/en?q='+word)
}