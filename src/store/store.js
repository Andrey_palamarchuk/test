import {makeObservable, observable, computed, action} from "mobx"
import words from '../asets/words.json'

class GlobalStore {
    wordsList = []
    activeList = []
    letter = ''

    constructor() {
        makeObservable(this, {
            wordsList: observable,
            letter: observable,
            activeList: observable,
            getCountStartLetter: computed,
            getCountAllLetterRepeat: computed,
            getEndLetter: computed,
            getCountDuplicateLetter: computed,
            saveList: action,
            fetch: action,
            searchList: action,
            setLetter: action,
        })
    }

    get getCountStartLetter() {
        return this.letter ? this.activeList.filter((item) => (item[0] === this.letter)).length : 0
    }

    get getCountAllLetterRepeat() {
        return this.letter ? this.activeList.filter((item) => (item.includes(this.letter))).length : 0
    }

    get getEndLetter() {
        return this.letter ? this.activeList.filter((item) => (item[item.length - 1] === this.letter)).length : 0
    }

    get getCountDuplicateLetter() {
        return this.letter ? this.activeList.filter((item) => (item.includes(this.letter + this.letter))).length : 0
    }

    setLetter(letter) {
        this.letter = letter
    }

    searchList(word) {
        if (word.length) {
            this.activeList = this.wordsList.filter((item) => (
                item.includes(word)
            ))
        } else {
            this.activeList = []
        }
    }

    saveList() {
        this.wordsList = words
    }

    fetch() {
        this.saveList(words)

    }
}

export default new GlobalStore